using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GunShooting : GrabbableObject
{
    public Transform spawnPoint;
    public float shootingForce;
    public GameObject bulletPrefab;
    public override void OnInteractionStart()
    {
        //base.OnInteractionStart();
        GameObject shot = Instantiate(bulletPrefab, spawnPoint.position, spawnPoint.rotation);
        shot.GetComponent<Rigidbody>().AddForce(shot.transform.forward * shootingForce, ForceMode.Impulse);

        shot.tag = "Bullet";
        Destroy(shot, 5);
    }
    public override void OnInteractionStop()
    {
        base.OnInteractionStop();
    }
    public override void OnInteractionUpdating()
    {
        base.OnInteractionUpdating();
    }

}