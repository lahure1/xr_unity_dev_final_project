using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public float moveSpeed;
    public float moveAmount;
    public SpawnArea spawnArea;

    float startPosition;

    private void Start()
    {
        startPosition = transform.position.x;


    }

    private void Update()
    {
        var newPosition = transform.position;

        newPosition.z = startPosition + Mathf.Sin(Time.time * moveSpeed) * moveAmount;

        transform.position = newPosition;
    }

    private void OnCollisionEnter(Collision collision)
    {
        var food = collision.gameObject.GetComponent<GrabbableObject>();

        if (food != null)
        {
            print("hit");
            Destroy(food.gameObject);
            Destroy(this.gameObject);

            spawnArea.SpawnTarget();
        }
    }
}
