using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TouchButton : MonoBehaviour
{
    public Transform button;
    public Transform downButton;
    public AudioSource buttonAudio;

    public UnityEvent OnButtonPressed;

    Vector3 originalStartPos;

    public GameObject rain;

     public bool rainOn = false;
    void Start()
    {
        originalStartPos = button.position;
        rain.SetActive(rainOn);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            button.position = downButton.position;
            buttonAudio.Play();

            OnButtonPressed.Invoke();

            rainOn = !rainOn;
            rain.SetActive(rainOn);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            button.position = originalStartPos;
        }
    }
}
