using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class continuousMovement : MonoBehaviour
{
    VRInput controller;

    public Transform xrRig;
    public float speed = 2f;

    Vector3 playerForward;
    Vector3 playerRight;
    void Start()
    {
        controller = GetComponent<VRInput>();
    }

    // Update is called once per frame
    void Update()
    {
        playerForward = controller.transform.forward;
        playerForward.y = 0;
        playerForward.Normalize();

        playerRight = controller.transform.right;
        playerRight.y = 0;
        playerRight.Normalize();

        xrRig.position += playerForward * controller.thumbStick.y * speed * Time.deltaTime;
        xrRig.position += playerRight * controller.thumbStick.x * speed * Time.deltaTime;
    }
}
