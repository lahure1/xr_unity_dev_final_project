using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeMaterials : MonoBehaviour
{
    public Material pressedMaterial;
    public Material originalMaterial;
    public List<GameObject> gameObjects;
    public Light light;
    public AudioSource music;

    bool switchMaterial = false;
    bool dark = false;
    bool musicPlaying = false;

    float originalIntensity;

    private void Start()
    {
        originalIntensity = light.intensity;
    }

    public void doChangeMaterials()
    {
        switchMaterial = !switchMaterial;

        if (switchMaterial)
        {
            for (int i = 0; i < gameObjects.Count; i++)
            {
                gameObjects[i].GetComponent<Renderer>().material = pressedMaterial;
            }
        }
        else
        {
            for (int i = 0; i < gameObjects.Count; i++)
            {
                gameObjects[i].GetComponent<Renderer>().material = originalMaterial;
            }
        }
    }

    public void changeLight()
    {
        dark = !dark;

        if (dark)
        {
            light.intensity = 0f;
        }
        else
        {
            light.intensity = originalIntensity;
        }
    }

    public void playNoRain()
    {
        if (music.isPlaying)
        {
            music.Stop();
        }
        else
        {
            music.Play();
        }
    }

}
