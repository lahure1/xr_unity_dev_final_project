using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRGrab : MonoBehaviour
{
    public GrabbableObject collidingObject;
    public GrabbableObject heldObject;

    VRInput controller;

    private void Start()
    {
        controller = GetComponent<VRInput>();
    }

    private void OnTriggerEnter(Collider other)
    {
        var grab = other.GetComponent<GrabbableObject>();
        if (grab != null)
        {
            collidingObject = grab;
            collidingObject.OnHoverStarted();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        var grab = other.GetComponent<GrabbableObject>();
        if (grab == collidingObject)
        {
            collidingObject?.OnHoverEnded();
            collidingObject = null;
        }
    }

    public void Grab()
    {
        if (collidingObject != null)
        {
            heldObject = collidingObject;
            heldObject.Grab(controller);

            controller.OnTriggerDown.AddListener(heldObject.OnInteractionStart);
            controller.OnTriggerUp.AddListener(heldObject.OnInteractionStop);
            controller.OnTriggerUpdating.AddListener(heldObject.OnInteractionUpdating);
        }
    }
    public void Release()
    {
        if (heldObject != null)
        {
            heldObject.Release(controller);
            heldObject.grabbedBody.velocity = controller.velocity;
            heldObject.grabbedBody.angularVelocity = controller.angularVelocity;
            controller.OnTriggerDown.RemoveListener(heldObject.OnInteractionStart);
            controller.OnTriggerUp.RemoveListener(heldObject.OnInteractionStop);
            controller.OnTriggerUpdating.RemoveListener(heldObject.OnInteractionUpdating);
            heldObject = null;
        }
    }
}
