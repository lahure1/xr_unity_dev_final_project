using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbableObject : MonoBehaviour
{
    public Color hoverColour;
    public Color nonHoverColour;

    Material material;
    public Rigidbody grabbedBody;

    public MeshRenderer meshRenderer;

    // Start is called before the first frame update
    void Awake()
    {
        if (meshRenderer != null)
        {
            material = meshRenderer.material;
        }
        else
        {
            material = GetComponent<MeshRenderer>().material;
        }
        grabbedBody = GetComponent<Rigidbody>();
    }

    public void OnHoverStarted()
    {
        material.color = hoverColour;
    }

    public void OnHoverEnded()
    {
        material.color = nonHoverColour;
    }

    public void Grab(VRInput controller)
    {
        FixedJoint fx = controller.gameObject.AddComponent<FixedJoint>();
        fx.connectedBody = grabbedBody;
        grabbedBody.useGravity = false;
    }

    public void Release(VRInput controller)
    {
        FixedJoint fx = controller.GetComponent<FixedJoint>();
        Destroy(fx);
        grabbedBody.useGravity = true;
    }

    public virtual void OnInteractionStart()
    {

    }

    public virtual void OnInteractionStop()
    {

    }

    public virtual void OnInteractionUpdating()
    {

    }
}
