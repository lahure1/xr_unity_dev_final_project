using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRWalk : MonoBehaviour
{
    public float walkSpeed = 5f;
    public GameObject XRrig;
    public GameObject VRCamera;
    public float maxvelocity = 2f;
    public float jumpVel = 20f;

    string LStickAxis;
    float LStickValueV;
    float LStickValueH;

    // Start is called before the first frame update
    void Start()
    {
        LStickAxis = "XRI_Left_Primary2DAxis_";
    }

    // Update is called once per frame
    void Update()
    {
        LStickValueV = Input.GetAxis(LStickAxis + "Vertical") * -1;
        LStickValueH = Input.GetAxis(LStickAxis + "Horizontal");

        Rigidbody r = this.GetComponent<Rigidbody>();

        Vector3 angles = VRCamera.transform.eulerAngles;

        this.transform.rotation = Quaternion.Euler(0, angles.y, 0);

        Vector3 move = new Vector3(LStickValueH, 0, LStickValueV);

        if (r?.velocity.x < maxvelocity && r?.velocity.z < maxvelocity && r?.velocity.x > maxvelocity * -1 && r?.velocity.z > maxvelocity * -1)
        {
            r?.AddRelativeForce(move * Time.deltaTime * walkSpeed, ForceMode.VelocityChange);
        }

        if (Input.GetButtonDown("XRI_Right_PrimaryButton") && r?.velocity.y == 0)
        {
            r?.AddRelativeForce(new Vector3(0, jumpVel, 0), ForceMode.Impulse);
        }

        if (Input.GetButtonDown("XRI_Right_SecondaryButton"))
        {
            this.transform.position = new Vector3(0, 1, 0);
        }
        XRrig.transform.position = this.transform.position;
        
    }
}
