using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buildMenu : MonoBehaviour
{
    public static bool BuildMode = true;

    public GameObject selectedObject;
    public GameObject menuPrefab;
    public GameObject menuHand;
    public LineRenderer menuLine;
    public Color highlightMaterial;
    public BuildControlls buildcontroll;
    Color previousMaterial;
    GameObject highlightedObject;
    public GameObject activeMenu;
    bool highlighting = false;
    int pageNumber = 1;
    int maxperpage = 6;
    float widthPerItem = 0.8f;
    List<MenuItem> currentMenuItems = new List<MenuItem>();
    public List<GameObject> buildingPrefabs;

    private float menuItemScale = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        activeMenu = Instantiate(menuPrefab, Vector3.zero, Quaternion.Euler(0, 0, 0));
        activeMenu.active = false;
        menuLine.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (BuildMode)
        {
            if (activeMenu.active)
            {
                activeMenu.transform.position = transform.position + Vector3.forward * 3;
                activeMenu.transform.position = activeMenu.transform.position + new Vector3(0, 2, 0);
                activeMenu.transform.LookAt(transform);

                RaycastHit hit;

                if (Physics.Raycast(menuHand.transform.position, menuHand.transform.forward, out hit))
                {
                    menuLine.SetPosition(0, menuHand.transform.position);
                    menuLine.SetPosition(1, hit.point);

                    if (hit.collider.gameObject.tag == "Can Select" && highlightedObject == null)
                    {
                        highlightedObject = hit.collider.gameObject;
                        Debug.Log("Hit Selectable Object");
                        highlighting = true;
                    }
                    else { highlighting = false; }

                    if (highlightedObject != null && !highlighting)
                    {
                        highlightedObject = null;
                    }
                }
            }

            if (Input.GetButtonDown("XRI_Left_SecondaryButton"))
            {
                if (selectedObject != null) { selectedObject.GetComponent<MeshRenderer>().material.color = previousMaterial; }
                selectedObject = highlightedObject;
                foreach (MenuItem m in currentMenuItems)
                {
                    try
                    {
                        if (m.menuObject == selectedObject)
                        {
                            buildcontroll.ChangeItem(m.preFabObject);
                        }
                        else if (m.menuObject == selectedObject.transform.parent.gameObject)
                        {
                            buildcontroll.ChangeItem(m.preFabObject);
                        }
                    }
                    catch { }
                }
                try
                {
                    previousMaterial = selectedObject.GetComponent<MeshRenderer>().material.color;
                    selectedObject.GetComponent<MeshRenderer>().material.color = highlightMaterial;
                }
                catch
                { }
            }

            if (Input.GetButtonDown("XRI_Left_PrimaryButton"))
            {
                activeMenu.active = !activeMenu.active;
                menuLine.enabled = activeMenu.active;

                if (!activeMenu.active)
                {
                    Debug.Log(currentMenuItems.Count);
                    foreach (MenuItem j in currentMenuItems)
                    {
                        Destroy(j.menuObject);
                        Debug.Log("pew");
                    }
                    currentMenuItems = new List<MenuItem>();
                    highlightedObject = null;
                    highlighting = false;
                }
                else
                {
                    spawnItemsInMenu();
                }
            }

            if (Input.GetButtonDown("XRI_Left_TriggerButton")) { PageOver(); }
        }

        if (Input.GetButtonDown("XRI_Left_Primary2DAxisClick")) { BuildMode = !BuildMode; }
    }

    public void spawnItemsInMenu()
    {
        for (int i = 0; i < 6; i++)
        {
            if (i + (pageNumber - 1) < buildingPrefabs.Count)
            {
                GameObject j = Instantiate(buildingPrefabs[i + (pageNumber - 1)], activeMenu.transform);
                j.tag = "Can Select";
                if (j.GetComponent<MeshFilter>() == null) 
                {
                    j.transform.GetChild(0).localScale = ResizeMenuItem(j);
                    j.transform.GetChild(0).tag = "Can Select";
                    Debug.Log("setting postion to zero");
                    j.transform.GetChild(0).localPosition = Vector3.zero;
                    Debug.Log("Set position to zero");
                }
                else { j.transform.localScale = ResizeMenuItem(j); }
                j.transform.position = activeMenu.transform.position + new Vector3(-2 + (widthPerItem * i), 0, 0);
                MenuItem m = new MenuItem(buildingPrefabs[i + (pageNumber - 1)]);
                m.menuObject = j;
                currentMenuItems.Add(m);
            }
        }
    }
    
    public Vector3 ResizeMenuItem(GameObject g)
    {
        var refrenceSize = buildingPrefabs[0].GetComponent<MeshFilter>().sharedMesh.bounds.size;

        Vector3 objSize;
        Debug.Log("Called Resize");
        if (g.GetComponent<MeshFilter>() == null) 
        {
            Debug.Log("Found null MeshFilter");
            objSize = g.GetComponentInChildren<MeshFilter>().sharedMesh.bounds.size;
            Debug.Log(objSize);
        }
        else 
        { 
            objSize = g.GetComponent<MeshFilter>().mesh.bounds.size;
        }
        Debug.Log("x: " + (refrenceSize.x / objSize.x) * menuItemScale + " y: " + (refrenceSize.y / objSize.y) * menuItemScale + " z: " + (refrenceSize.z / objSize.z) * menuItemScale);
        return new Vector3((refrenceSize.x / objSize.x) * menuItemScale, (refrenceSize.y / objSize.y) * menuItemScale, (refrenceSize.z / objSize.z) * menuItemScale);
    }

    public void PageOver()
    {
        pageNumber += 6;

        if (pageNumber > buildingPrefabs.Count && pageNumber - 6 > buildingPrefabs.Count)
        {
            pageNumber = 1;
        }

        Debug.Log(currentMenuItems.Count);
        foreach (MenuItem j in currentMenuItems)
        {
            Destroy(j.menuObject);
            Debug.Log("pew");
        }
        currentMenuItems = new List<MenuItem>();
        highlightedObject = null;
        highlighting = false;

        spawnItemsInMenu();
    }
}
