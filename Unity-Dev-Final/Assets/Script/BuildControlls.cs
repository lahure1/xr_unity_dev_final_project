using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildControlls : MonoBehaviour
{
    GameObject currentItem;
    GameObject preFab;
    public buildMenu menu;
    public LineRenderer line;
    public float rotationSpeed = 2f;
    // Start is called before the first frame update
    void Start()
    {
        line.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (buildMenu.BuildMode)
        {
            if (currentItem != null)
            {
                if (!line.enabled)
                {
                    line.enabled = true;
                    currentItem.SetActive(true);
                }
                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.forward, out hit))
                {
                    Vector3 hitPosition;
                    hitPosition = hit.point;
                    line.SetPosition(0, transform.position);
                    line.SetPosition(1, hitPosition);
                    float x = Mathf.RoundToInt(hitPosition.x * 10) / 10;
                    float z = Mathf.RoundToInt(hitPosition.z * 10) / 10;
                    float ySize;

                    if (currentItem.GetComponent<MeshFilter>() == null) { ySize = currentItem.GetComponentInChildren<MeshFilter>().sharedMesh.bounds.size.y; }
                    else { ySize = currentItem.GetComponent<MeshFilter>().mesh.bounds.size.y; }
                    currentItem.transform.position = new Vector3(x, hitPosition.y + (ySize / 2), z);
                }

                if (Input.GetButtonDown("XRI_Right_TriggerButton"))
                {
                    Instantiate(preFab, currentItem.transform.position, currentItem.transform.rotation);
                }

                float thumbStick = Input.GetAxis("XRI_Right_Primary2DAxis_Horizontal");

                currentItem.transform.rotation = Quaternion.Euler(currentItem.transform.rotation.eulerAngles + Vector3.up * thumbStick * rotationSpeed * Time.deltaTime);
            }
        }
        else if (line.enabled)
        {
            line.enabled = false;
            currentItem.SetActive(false);
        }
    }
    public void ChangeItem(GameObject g)
    {
        if (currentItem != null) { Destroy(currentItem); }
        currentItem = Instantiate(g, transform.position, Quaternion.Euler(new Vector3(0, 0, 0)));
        Destroy(currentItem.GetComponent<Collider>());
        preFab = g;
    }
}
