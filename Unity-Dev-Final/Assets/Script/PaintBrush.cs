using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintBrush : GrabbableObject
{
    public GameObject PaintStroke;
    private GameObject SpawnedPaint;
    private PaintBrushTip paintBrushTip;

    private void Start()
    {
        paintBrushTip = GetComponentInChildren<PaintBrushTip>();
    }

    public override void OnInteractionStart()
    {
        SpawnedPaint = Instantiate(PaintStroke, paintBrushTip.transform.position, paintBrushTip.transform.rotation);

        TrailRenderer paintTrail = SpawnedPaint.GetComponentInChildren<TrailRenderer>();

        paintTrail.material = paintBrushTip.PaintMaterial;
    }

    public override void OnInteractionUpdating()
    {
        if (SpawnedPaint)
        {
            SpawnedPaint.transform.position = paintBrushTip.transform.position;
        }
    }
    public override void OnInteractionStop()
    {
        SpawnedPaint.transform.position = SpawnedPaint.transform.position;
        SpawnedPaint = null;
    }
}
