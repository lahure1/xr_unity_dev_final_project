using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableTrees : MonoBehaviour
{
    public Lever upDownLever;
    public float speed = 2f;

    public float deadZone = 0.15f;

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(upDownLever.normalizeAngle()) > deadZone)
        {
            transform.position = transform.position + transform.up * Time.deltaTime * speed * upDownLever.normalizeAngle();
        }
    }
}
