using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : GrabbableObject
{

    public HingeJoint leverJoint;

    void Start()
    {
        grabbedBody.centerOfMass = Vector3.zero;
    }

    public float normalizeAngle()
    {
        float normAngle = leverJoint.angle / leverJoint.limits.max;
        return normAngle;
    }
}
