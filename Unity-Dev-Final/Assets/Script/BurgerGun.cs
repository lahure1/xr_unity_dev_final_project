using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurgerGun : GrabbableObject
{
    public Transform spawnPoint;
    public float shootingForce;
    public GameObject foodPrefab;

    public override void OnInteractionStart()
    {
        GameObject shot = Instantiate(foodPrefab, spawnPoint.position, spawnPoint.rotation);

        shot.GetComponent<Rigidbody>().AddForce(shot.transform.forward * shootingForce);

        Destroy(shot, 5);
    }

    public override void OnInteractionStop()
    {
        
    }

    public override void OnInteractionUpdating()
    {
        
    }
}
