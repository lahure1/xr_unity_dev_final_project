using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : GrabbableObject
{
    public Light source;
    public override void OnInteractionStart()
    {
        source.enabled = !source.enabled;
    }

    void Start()
    {
        source = GetComponentInChildren<Light>();
        source.enabled = false;
    }
}
