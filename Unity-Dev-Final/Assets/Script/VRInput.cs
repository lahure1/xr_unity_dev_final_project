using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class VRInput : MonoBehaviour
{
    public Hand hand = Hand.Left;
    public UnityEvent OnGripDown;
    public UnityEvent OnGripUp;

    public UnityEvent OnTriggerDown;
    public UnityEvent OnTriggerUp;
    public UnityEvent OnTriggerUpdating;

    public UnityEvent thumbStickButtonDown;
    public UnityEvent thumbStickButtonUp;
    public UnityEvent thumbStickButtonUpdating;

    float triggerValue;
    string triggerAxis;
    string triggerButton;

    float gripValue;
    string gripAxis;
    string gripButton;

    Animator handAnimator;

    public Vector2 thumbStick;
    string thumbStickX;
    string thumbStickY;

    string thumbStickButton;

    public Vector3 velocity;
    Vector3 previousPosition;
    public Vector3 angularVelocity;
    Vector3 previousAngularVelcoity;

    // Start is called before the first frame update
    void Start()
    {
        gripAxis = "XRI_" + hand + "_Grip";
        gripButton = "XRI_" + hand + "_GripButton";

        triggerAxis = "XRI_" + hand + "_Trigger";
        triggerButton = "XRI_" + hand + "_TriggerButton";

        thumbStickY = "XRI_" + hand + "_Primary2DAxis_Vertical";
        thumbStickX = "XRI_" + hand + "_Primary2DAxis_Horizontal";

        thumbStickButton = "XRI_" + hand + "_Primary2DAxisClick";

        handAnimator = GetComponentInChildren<Animator>();
        velocity = transform.position;
        angularVelocity = transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        gripValue = Input.GetAxis(gripAxis);

        thumbStick = new Vector2(Input.GetAxis(thumbStickX), Input.GetAxis(thumbStickY));

        velocity = (transform.position - previousPosition) / Time.deltaTime;
        previousPosition = transform.position;

        angularVelocity = (transform.eulerAngles - previousAngularVelcoity) / Time.deltaTime;
        previousAngularVelcoity = transform.eulerAngles;

        if (handAnimator != null)
        {
            handAnimator.Play("Gripped", 0, gripValue);
        }

        if (Input.GetButtonDown(gripButton))
        {
            OnGripDown?.Invoke();
        }

        if (Input.GetButtonUp(gripButton))
        {
            OnGripUp?.Invoke();
        }

        if (Input.GetButtonDown(triggerButton))
        {
            OnTriggerDown?.Invoke();
        }

        if (Input.GetButtonUp(triggerButton))
        {
            OnTriggerUp?.Invoke();
        }

        if (Input.GetButton(triggerButton))
        {
            OnTriggerUpdating?.Invoke();
        }

        if (Input.GetButtonDown(thumbStickButton))
        {
            thumbStickButtonDown?.Invoke();
        }

        if (Input.GetButtonUp(thumbStickButton))
        {
            thumbStickButtonUp?.Invoke();
        }

        if (Input.GetButton(thumbStickButton))
        {
            thumbStickButtonUpdating?.Invoke();
        }
    }

}

public enum Hand
{
    Left,
    Right
}