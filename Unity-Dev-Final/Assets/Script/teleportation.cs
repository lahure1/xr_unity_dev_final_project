using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class teleportation : MonoBehaviour
{
    public Transform xrRig;
    public ParticleSystem particles;
    public float midPointHeight = 2f;
    public int lineResolution = 10;
    public GameObject reticlePrefab;

    private Vector3 hitPosition;
    private VRInput controller;
    private LineRenderer line;
    private GameObject reticle;

    bool canTeleport = false;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<VRInput>();

        line = GetComponent<LineRenderer>();
        line.positionCount = lineResolution;
        line.enabled = false;

        if (reticle == null)
        {
            reticle = Instantiate(reticlePrefab);
            reticle.SetActive(false);
        }

        controller.thumbStickButtonUpdating.AddListener(RaycastLine);
        controller.thumbStickButtonUp.AddListener(Teleport);

        particles.enableEmission = false;
    }

    public void RaycastLine()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            hitPosition = hit.point;
            curveLine(hitPosition);
            particles.transform.position = hitPosition;
            particles.enableEmission = true;
            line.enabled = true;
            canTeleport = true;

            reticle.transform.position = hitPosition;
            reticle.transform.LookAt(hit.normal + hitPosition);
            reticle.SetActive(true);

        }else { canTeleport = false; line.enabled = false; particles.enableEmission = false; reticle.SetActive(false); }
    }

    public void Teleport()
    {
        if (canTeleport)
        {
            xrRig.position = hitPosition;
            canTeleport = false;
            line.enabled = false;
            particles.enableEmission = false;
            reticle.SetActive(false);
        }
    }

    private void curveLine(Vector3 hitPoint)
    {
        Vector3 startPoint = controller.transform.position;
        Vector3 endPoint = hitPoint;
        Vector3 midPoint = (endPoint + startPoint) / 2;
        midPoint.y += midPointHeight;

        for (int i = 0; i < lineResolution; i++)
        {
            float t = (float)i / ((float)lineResolution - 1f);

            Vector3 startToMid = Vector3.Lerp(startPoint, midPoint, t);
            Vector3 midToEnd = Vector3.Lerp(midPoint, endPoint, t);
            Vector3 curvePos = Vector3.Lerp(startToMid, midToEnd, t);

            line.SetPosition(i, curvePos);
        }
    }
}
