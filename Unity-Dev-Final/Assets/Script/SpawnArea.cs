using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnArea : MonoBehaviour
{
    Collider SpawnAreaCollider;
    public GameObject TargetPrefab;

    private void Start()
    {
        SpawnAreaCollider = GetComponent<BoxCollider>();
    }

    private Vector3 GetRandomPosition()
    {
        var xValue = Random.Range(SpawnAreaCollider.bounds.min.x, SpawnAreaCollider.bounds.max.x);
        var yValue = Random.Range(SpawnAreaCollider.bounds.min.y, SpawnAreaCollider.bounds.max.y);
        var zValue = Random.Range(SpawnAreaCollider.bounds.min.z, SpawnAreaCollider.bounds.max.z);

        return new Vector3(xValue, yValue, zValue);
    }

    public void SpawnTarget()
    {
        var newTarget = Instantiate(TargetPrefab, GetRandomPosition(), transform.rotation);
        newTarget.transform.eulerAngles = new Vector3(0, 0, 90);
        newTarget.GetComponent<Target>().spawnArea = this;
    }
}
