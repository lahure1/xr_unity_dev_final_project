using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintBrushTip : MonoBehaviour
{
    public Material OriginalMaterial;
    public Material PaintMaterial;

    private void Start()
    {
        OriginalMaterial = GetComponent<Renderer>().material;
        PaintMaterial = OriginalMaterial;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Paint")
        {
            PaintMaterial = collision.gameObject.GetComponent<Renderer>().material;
            GetComponent<Renderer>().material = PaintMaterial;
        }
    }
}
