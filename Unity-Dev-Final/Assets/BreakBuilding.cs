using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BreakBuilding : MonoBehaviour
{
    public GameObject crackedBuilding;
    public GameObject originalPrefab;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            Instantiate(crackedBuilding, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
            Destroy(originalPrefab);
        }
        //var bullet = collision.gameObject.GetComponent<GameObject>();
        //if(bullet != null)
        //{
        //    Instantiate(crackedBuilding, collision.gameObject.transform.position, collision.gameObject.transform.rotation);
        //    Destroy(originalPrefab);
        //}
    }
}